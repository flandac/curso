<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profesion;

class ProfesionController extends Controller
{
    public function index(){
        /*Obtener todas las profesiones*/
        $profesiones=Profesion::get();
        
    return view ('profesion.index',[
        'profesiones'=>$profesiones,

    ]);
        
    }
}
